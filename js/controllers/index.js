var Index = {
    getSlider: function(){
        $.ajax({
            url: url.base_url("slider")+"getDataSliderWeb",
            async: false,
            dataType: 'html',
            type: 'POST',
            error: function(){
                alert("Gagal Jaringan");
            },

            success: function(resp){
                $('div#content_wrapper').html(resp);
            }
        });
    },
    
    getDataKontakWa: function(){
        $.ajax({
            url: url.base_url("kontak")+"getDataWeb",
            async: false,
            dataType: 'html',
            type: 'POST',
            error: function(){
                alert("Gagal Jaringan");
            },

            success: function(resp){
                $('div#div_kontak_wa').html(resp);
            }
        });
    },

    gotoWhatsap: function(elm){
        var no_wa = $('#kontak_wa').val();
        var url = "https://api.whatsapp.com/send?phone="+no_wa+"&text=Saya Tertarik";
        window.location.href = url;
    }
};

$(function(){
    Index.getSlider();
    Index.getDataKontakWa();
});