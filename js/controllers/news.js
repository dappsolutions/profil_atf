var News = {
    getData: function () {
        $.ajax({
            url: url.base_url("news") + "getDataWebLimit",
            async: false,
            dataType: 'json',
            type: 'POST',
            error: function () {
                alert("Gagal Jaringan");
            },

            success: function (resp) {
                $('div#news').html(resp.view);
            }
        });
    },
    
    getRelateNews: function () {
        $.ajax({
            url: url.base_url("news") + "getRelateNews",
            async: false,
            dataType: 'json',
            type: 'POST',
            error: function () {
                alert("Gagal Jaringan");
            },

            success: function (resp) {
                // $('div#news').html(resp.view);
            }
        });
    },

    setDetail: function (elm, e) {
        e.preventDefault();
        var id = $(elm).attr('data_id');

        console.log('data_id', id);
        $.ajax({
            url: url.base_url("news") + "setSessionId/"+id,
            async: false,
            dataType: 'html',
            type: 'POST',
            error: function () {
                alert("Gagal Jaringan");
            },

            success: function (resp) {
                window.location.href = url.base_url_dashboard("news_1.html");
            }
        });
    },

    getDetail: function () {
        $.ajax({
            url: url.base_url("news") + "getDetailNews",
            async: false,
            dataType: 'json',
            type: 'POST',
            error: function () {
                alert("Gagal Jaringan");
            },

            success: function (resp) {
                $('div#content-detail').html(resp.view);
            }
        });
    },
};

$(function () {
    News.getData();
    News.getDetail();
    // News.getRelateNews();

});